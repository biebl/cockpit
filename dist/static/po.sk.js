window.cockpit_po = {
 "": {
  "plural-forms": (n) => (n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2,
  "language": "sk",
  "language-direction": "ltr"
 },
 "$0 GiB": [
  null,
  "$0 GiB"
 ],
 "$0 day": [
  null,
  "$0 deň",
  "$0 dni",
  "$0 dní"
 ],
 "$0 error": [
  null,
  "$0 chyba"
 ],
 "$0 exited with code $1": [
  null,
  "$0 skončilo s kódom $1"
 ],
 "$0 failed": [
  null,
  "$0 sa nepodarilo"
 ],
 "$0 hour": [
  null,
  "$0 hodina",
  "$0 hodiny",
  "$0 hodín"
 ],
 "$0 is not available from any repository.": [
  null,
  "$0 nie je k dispozícií v žiadom repozitári."
 ],
 "$0 key changed": [
  null,
  "$0 kľúč sa zmenil"
 ],
 "$0 killed with signal $1": [
  null,
  "$0 nútene ukončené signálom $1"
 ],
 "$0 minute": [
  null,
  "$0 minúta",
  "$0 minúty",
  "$0 minút"
 ],
 "$0 month": [
  null,
  "$0 mesiac",
  "$0 mesiace",
  "$0 mesiacov"
 ],
 "$0 week": [
  null,
  "$0 týždeň",
  "$0 týždne",
  "$0 týždňov"
 ],
 "$0 will be installed.": [
  null,
  "$0 bude nainštalovaný."
 ],
 "$0 year": [
  null,
  "$0 rok",
  "$0 roky",
  "$0 rokov"
 ],
 "1 day": [
  null,
  "1 deň"
 ],
 "1 hour": [
  null,
  "1 hodina"
 ],
 "1 minute": [
  null,
  "1 minúta"
 ],
 "1 week": [
  null,
  "1 týždeň"
 ],
 "20 minutes": [
  null,
  "20 minút"
 ],
 "40 minutes": [
  null,
  "40 minút"
 ],
 "5 minutes": [
  null,
  "5 minút"
 ],
 "6 hours": [
  null,
  "6 hodín"
 ],
 "60 minutes": [
  null,
  "60 minút"
 ],
 "A modern browser is required for security, reliability, and performance.": [
  null,
  "Z dôvodu zabezpečenia, spoľahlivosti a rýchlosti je potrebný moderný prehliadač."
 ],
 "Absent": [
  null,
  "Chýba"
 ],
 "Accept key and log in": [
  null,
  "Prijať kľúč a prihlásiť sa"
 ],
 "Add $0": [
  null,
  "Pridať $0"
 ],
 "Additional packages:": [
  null,
  "Ďalšie balíky:"
 ],
 "Administration with Cockpit Web Console": [
  null,
  "Správa pomocou webovej konzole Cockpit"
 ],
 "Advanced TCA": [
  null,
  "Pokročilé TCA"
 ],
 "All-in-one": [
  null,
  "All-in-one"
 ],
 "Ansible": [
  null,
  "Ansible"
 ],
 "Ansible roles documentation": [
  null,
  "Dokumentácia k Ansible roliam"
 ],
 "Authentication failed": [
  null,
  "Overenie sa nepodarilo"
 ],
 "Authentication failed: Server closed connection": [
  null,
  ""
 ],
 "Authentication is required to perform privileged tasks with the Cockpit Web Console": [
  null,
  ""
 ],
 "Automatically using NTP": [
  null,
  "Automaticky využitím NTP"
 ],
 "Automatically using specific NTP servers": [
  null,
  "Automaticky využitím konkrétnych NTP serverov"
 ],
 "Automation script": [
  null,
  "Automatizačný skript"
 ],
 "Blade": [
  null,
  "Blade"
 ],
 "Blade enclosure": [
  null,
  "Skriňa pre blade servery"
 ],
 "Bus expansion chassis": [
  null,
  ""
 ],
 "Bypass browser check": [
  null,
  ""
 ],
 "Cancel": [
  null,
  "Zrušiť"
 ],
 "Cannot forward login credentials": [
  null,
  "Nie je možné preposlať prístupové údaje"
 ],
 "Cannot schedule event in the past": [
  null,
  "Nie je možné plánovať udalosti do minulosti"
 ],
 "Change": [
  null,
  "Zmeniť"
 ],
 "Change system time": [
  null,
  "Zmeniť systémový čas"
 ],
 "Changed keys are often the result of an operating system reinstallation. However, an unexpected change may indicate a third-party attempt to intercept your connection.": [
  null,
  "Zmenené kľúče sú často výsledkom preinštalovania operačného systému. Avšak neočakávaná zmena môže znamenať, že sa tretia strana snaží odpočúvať vaše spojenie."
 ],
 "Checking installed software": [
  null,
  "Zisťuje sa nainštalovaný software"
 ],
 "Close": [
  null,
  "Zavrieť"
 ],
 "Cockpit": [
  null,
  "Cockpit"
 ],
 "Cockpit authentication is configured incorrectly.": [
  null,
  ""
 ],
 "Cockpit configuration of NetworkManager and Firewalld": [
  null,
  ""
 ],
 "Cockpit could not contact the given host.": [
  null,
  "Cockpitu sa nepodarilo kontaktovať daného hostiteľa."
 ],
 "Cockpit is a server manager that makes it easy to administer your Linux servers via a web browser. Jumping between the terminal and the web tool is no problem. A service started via Cockpit can be stopped via the terminal. Likewise, if an error occurs in the terminal, it can be seen in the Cockpit journal interface.": [
  null,
  "Cockpit je správca serveru, který uľahčuje správu Linuxových serverov cez webový prehliadač. Nie je žiadným problémom prechádzať medzi terminálom a webovým nástrojom. Služba spustená cez Cockpit môže byť zastavená v termináli. Podobne, pokiaľ dôjde k chybe v termináli, je toto vidieť v rozhraní žurnálu v Cockpite."
 ],
 "Cockpit is not compatible with the software on the system.": [
  null,
  "Cockpit nie je kompatibilný so sofvérom na systéme."
 ],
 "Cockpit is not installed on the system.": [
  null,
  "Cockpit nie je nainštalovaný na tomto systéme."
 ],
 "Cockpit is perfect for new sysadmins, allowing them to easily perform simple tasks such as storage administration, inspecting journals and starting and stopping services. You can monitor and administer several servers at the same time. Just add them with a single click and your machines will look after its buddies.": [
  null,
  "Cockpit je skvelý nástroj pre nových správcov serverov, ktorým jednoducho umožňuje vykonávať úlohy ako správa úložiska, kontrola žurnálu a spúšťanie či zastavovanie služieb. Môžte monitorovať a spravovať viacero serverov naraz. Stačí ich pridať jedným kliknutím a vaše stroje sa budú starať o svojích kamarátov."
 ],
 "Cockpit might not render correctly in your browser": [
  null,
  ""
 ],
 "Collect and package diagnostic and support data": [
  null,
  ""
 ],
 "Collect kernel crash dumps": [
  null,
  ""
 ],
 "Compact PCI": [
  null,
  "Kompaktné PCI"
 ],
 "Connect to": [
  null,
  "Pripojiť k"
 ],
 "Connection has timed out.": [
  null,
  "Časový limit spojenia vypršal."
 ],
 "Convertible": [
  null,
  "Počítač 2v1"
 ],
 "Copy": [
  null,
  "Kopírovať"
 ],
 "Copy to clipboard": [
  null,
  ""
 ],
 "Create": [
  null,
  "Vytvoriť"
 ],
 "Create new task file with this content.": [
  null,
  ""
 ],
 "Ctrl+Insert": [
  null,
  "Ctrl+Insert"
 ],
 "Delay": [
  null,
  "Omeškanie"
 ],
 "Desktop": [
  null,
  "Desktop"
 ],
 "Detachable": [
  null,
  "Odpojiteľné"
 ],
 "Diagnostic reports": [
  null,
  "Diagnostické hlásenia"
 ],
 "Docking station": [
  null,
  "Dokovacia stanica"
 ],
 "Download a new browser for free": [
  null,
  "Stiahnite si zdarma nový prehliadač"
 ],
 "Downloading $0": [
  null,
  "Sťahuje sa $0"
 ],
 "Dual rank": [
  null,
  "Dual rank"
 ],
 "Embedded PC": [
  null,
  ""
 ],
 "Excellent password": [
  null,
  "Skvelé heslo"
 ],
 "Expansion chassis": [
  null,
  ""
 ],
 "Failed to change password": [
  null,
  "Nepodarilo sa zmeniť heslo"
 ],
 "Go to now": [
  null,
  "Prejsť na súčasnosť"
 ],
 "Handheld": [
  null,
  "Pre držanie do ruky"
 ],
 "Host key is incorrect": [
  null,
  "Kľúč stroja nie je správny"
 ],
 "If the fingerprint matches, click \"Accept key and log in\". Otherwise, do not log in and contact your administrator.": [
  null,
  ""
 ],
 "Install": [
  null,
  "Inštalovať"
 ],
 "Install software": [
  null,
  "Inštalovať software"
 ],
 "Installing $0": [
  null,
  "Inštaluje sa $0"
 ],
 "Internal error": [
  null,
  "Interná chyba"
 ],
 "Internal error: Invalid challenge header": [
  null,
  ""
 ],
 "Invalid date format": [
  null,
  "Neplatný formát dátumu"
 ],
 "Invalid date format and invalid time format": [
  null,
  "Neplatný formát dátumu a času"
 ],
 "Invalid file permissions": [
  null,
  ""
 ],
 "Invalid time format": [
  null,
  "Neplatný formát čase"
 ],
 "Invalid timezone": [
  null,
  "Neplatná časová zóna"
 ],
 "IoT gateway": [
  null,
  ""
 ],
 "Kernel dump": [
  null,
  ""
 ],
 "Laptop": [
  null,
  "Notebook"
 ],
 "Learn more": [
  null,
  "Zistiť viac"
 ],
 "Loading system modifications...": [
  null,
  "Načítavajú sa systémové zmeny..."
 ],
 "Log in": [
  null,
  "Prihlásiť"
 ],
 "Log in with your server user account.": [
  null,
  ""
 ],
 "Log messages": [
  null,
  "Záznamy udalostí"
 ],
 "Login again": [
  null,
  "Znovu prihlásiť"
 ],
 "Login failed": [
  null,
  "Prihlásenie sa nepodarilo"
 ],
 "Logout successful": [
  null,
  "Odhlásenie bolo úspešné"
 ],
 "Low profile desktop": [
  null,
  "Nízky desktop"
 ],
 "Lunch box": [
  null,
  "Kufríkový počítač"
 ],
 "Main server chassis": [
  null,
  "Hlavná skriňa serveru"
 ],
 "Manually": [
  null,
  "Ručne"
 ],
 "Message to logged in users": [
  null,
  ""
 ],
 "Mini PC": [
  null,
  ""
 ],
 "Mini tower": [
  null,
  ""
 ],
 "Multi-system chassis": [
  null,
  ""
 ],
 "NTP server": [
  null,
  "NTP server"
 ],
 "Need at least one NTP server": [
  null,
  ""
 ],
 "Networking": [
  null,
  "Sieť"
 ],
 "New host": [
  null,
  "Nový hostiteľ"
 ],
 "New password was not accepted": [
  null,
  "Nové heslo nebolo prijaté"
 ],
 "No delay": [
  null,
  ""
 ],
 "No such file or directory": [
  null,
  ""
 ],
 "No system modifications": [
  null,
  "Žiadne systémové zmeny"
 ],
 "Not a valid private key": [
  null,
  ""
 ],
 "Not permitted to perform this action.": [
  null,
  "Neoprávený k vykonaniu tejto akcie."
 ],
 "Not synchronized": [
  null,
  "Nezosynchronizované"
 ],
 "Notebook": [
  null,
  "Notebook"
 ],
 "Ok": [
  null,
  "Ok"
 ],
 "Old password not accepted": [
  null,
  "Pôvodné heslo nebolo prijaté"
 ],
 "Once Cockpit is installed, enable it with \"systemctl enable --now cockpit.socket\".": [
  null,
  "Keď bude Cockpit nainštalovaný, povoľte ho pomocou \"systemctl enable --now cockpit.socket\"."
 ],
 "Or use a bundled browser": [
  null,
  "Alebo použite pribalený prehliadač"
 ],
 "Other": [
  null,
  "Iný"
 ],
 "Other options": [
  null,
  "Iné voľby"
 ],
 "PackageKit crashed": [
  null,
  "PackageKit zhavaroval"
 ],
 "Password": [
  null,
  "Heslo"
 ],
 "Password is not acceptable": [
  null,
  "Heslo nie je prijatelné"
 ],
 "Password is too weak": [
  null,
  "Heslo je príliš slabé"
 ],
 "Password not accepted": [
  null,
  "Heslo nebolo prijaté"
 ],
 "Paste": [
  null,
  "Vložiť"
 ],
 "Path to file": [
  null,
  "Cesta k súboru"
 ],
 "Peripheral chassis": [
  null,
  ""
 ],
 "Permission denied": [
  null,
  "Prístup odmietnutý"
 ],
 "Pick date": [
  null,
  "Vybrať dátum"
 ],
 "Pizza box": [
  null,
  ""
 ],
 "Please enable JavaScript to use the Web Console.": [
  null,
  ""
 ],
 "Please specify the host to connect to": [
  null,
  "Zadajte stroj, ku ktorému sa chcete pripojiť"
 ],
 "Portable": [
  null,
  "Prenosný"
 ],
 "Present": [
  null,
  "Prítomné"
 ],
 "Prompting via ssh-add timed out": [
  null,
  ""
 ],
 "Prompting via ssh-keygen timed out": [
  null,
  ""
 ],
 "RAID chassis": [
  null,
  "RAID skriňa"
 ],
 "Rack mount chassis": [
  null,
  ""
 ],
 "Reboot": [
  null,
  "Reštartovať"
 ],
 "Refusing to connect. Host is unknown": [
  null,
  "Odmieta sa pripojenie. Stroj nie je známy"
 ],
 "Refusing to connect. Hostkey does not match": [
  null,
  "Odmieta sa pripojenie. Kľúč stroja sa líši"
 ],
 "Refusing to connect. Hostkey is unknown": [
  null,
  "Odmieta sa pripojenie. Kľúč stroja nie je známy"
 ],
 "Removals:": [
  null,
  "Odstránenia:"
 ],
 "Removing $0": [
  null,
  "Odstraňuje sa $0"
 ],
 "SELinux": [
  null,
  "SELinux"
 ],
 "Sealed-case PC": [
  null,
  "Počítač so zapäčatenou skriňou"
 ],
 "Security Enhanced Linux configuration and troubleshooting": [
  null,
  ""
 ],
 "Server": [
  null,
  "Server"
 ],
 "Server has closed the connection.": [
  null,
  "Server zavrel spojenie."
 ],
 "Set time": [
  null,
  "Nastaviť čas"
 ],
 "Shell script": [
  null,
  "Shellový skript"
 ],
 "Shift+Insert": [
  null,
  "Shift+Insert"
 ],
 "Shut down": [
  null,
  "Vypnúť"
 ],
 "Single rank": [
  null,
  "Single rank"
 ],
 "Space-saving computer": [
  null,
  "Miesto-šetriaci počítač"
 ],
 "Specific time": [
  null,
  "Konkrétny čas"
 ],
 "Stick PC": [
  null,
  ""
 ],
 "Storage": [
  null,
  "Úložisko"
 ],
 "Sub-Chassis": [
  null,
  "Podskriňa"
 ],
 "Sub-Notebook": [
  null,
  "Sub-Notebook"
 ],
 "Synchronized": [
  null,
  "Synchronizované"
 ],
 "Synchronized with $0": [
  null,
  "Synchronizované s $0"
 ],
 "Synchronizing": [
  null,
  "Synchronizuje sa"
 ],
 "Tablet": [
  null,
  "Tablet"
 ],
 "The logged in user is not permitted to view system modifications": [
  null,
  ""
 ],
 "The passwords do not match.": [
  null,
  "Heslá sa líšia."
 ],
 "The resulting fingerprint is fine to share via public methods, including email.": [
  null,
  ""
 ],
 "The server refused to authenticate '$0' using password authentication, and no other supported authentication methods are available.": [
  null,
  "Server odmietol overiť „$0“ pomocou overenia heslom a nie sú k dispozícií žiadne ďalšie metódy overenia."
 ],
 "The server refused to authenticate using any supported methods.": [
  null,
  ""
 ],
 "The web browser configuration prevents Cockpit from running (inaccessible $0)": [
  null,
  ""
 ],
 "This tool configures the SELinux policy and can help with understanding and resolving policy violations.": [
  null,
  ""
 ],
 "This tool configures the system to write kernel crash dumps to disk.": [
  null,
  ""
 ],
 "This tool generates an archive of configuration and diagnostic information from the running system. The archive may be stored locally or centrally for recording or tracking purposes or may be sent to technical support representatives, developers or system administrators to assist with technical fault-finding and debugging.": [
  null,
  ""
 ],
 "This tool manages local storage, such as filesystems, LVM2 volume groups, and NFS mounts.": [
  null,
  ""
 ],
 "This tool manages networking such as bonds, bridges, teams, VLANs and firewalls using NetworkManager and Firewalld. NetworkManager is incompatible with Ubuntu's default systemd-networkd and Debian's ifupdown scripts.": [
  null,
  ""
 ],
 "This web browser is too old to run the Web Console (missing $0)": [
  null,
  ""
 ],
 "Time zone": [
  null,
  "Časová zóna"
 ],
 "To ensure that your connection is not intercepted by a malicious third-party, please verify the host key fingerprint:": [
  null,
  ""
 ],
 "To verify a fingerprint, run the following on $0 while physically sitting at the machine or through a trusted network:": [
  null,
  ""
 ],
 "Toggle date picker": [
  null,
  ""
 ],
 "Too much data": [
  null,
  ""
 ],
 "Total size: $0": [
  null,
  "Celková veľkosť: $0"
 ],
 "Tower": [
  null,
  "Veža"
 ],
 "Try again": [
  null,
  "Skúsiť znova"
 ],
 "Trying to synchronize with $0": [
  null,
  ""
 ],
 "Unable to connect to that address": [
  null,
  ""
 ],
 "Unknown": [
  null,
  "Neznáme"
 ],
 "Untrusted host": [
  null,
  "Nedôveryhodnotný stroj"
 ],
 "User name": [
  null,
  "Užívateľské meno"
 ],
 "User name cannot be empty": [
  null,
  "Užívateľské meno nemôže byť prázdne"
 ],
 "Validating authentication token": [
  null,
  "Kontroluje sa overovací žetón"
 ],
 "View automation script": [
  null,
  "Ukázať automatizačný skript"
 ],
 "Waiting for other software management operations to finish": [
  null,
  "Čaká sa na dokončenie ostatných operácií správy balíčkov"
 ],
 "Web Console for Linux servers": [
  null,
  "Webová konzola pre linuxové servery"
 ],
 "Wrong user name or password": [
  null,
  "Chybné užívateľské meno alebo heslo"
 ],
 "You are connecting to $0 for the first time.": [
  null,
  ""
 ],
 "Your browser does not allow paste from the context menu. You can use Shift+Insert.": [
  null,
  ""
 ],
 "Your session has been terminated.": [
  null,
  ""
 ],
 "Your session has expired. Please log in again.": [
  null,
  ""
 ],
 "Zone": [
  null,
  "Zóna"
 ],
 "[binary data]": [
  null,
  "[binárne dáta]"
 ],
 "[no data]": [
  null,
  "[žiadne dáta]"
 ],
 "show less": [
  null,
  "ukázať menej"
 ],
 "show more": [
  null,
  "ukázať viac"
 ]
};
