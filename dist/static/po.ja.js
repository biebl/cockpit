window.cockpit_po = {
 "": {
  "plural-forms": (n) => 0,
  "language": "ja",
  "language-direction": "ltr"
 },
 "$0 GiB": [
  null,
  "$0 GiB"
 ],
 "$0 day": [
  null,
  "$0 日"
 ],
 "$0 error": [
  null,
  "$0 エラー"
 ],
 "$0 exited with code $1": [
  null,
  "$0 がコード $1 で終了しました"
 ],
 "$0 failed": [
  null,
  "$0 が失敗しました"
 ],
 "$0 hour": [
  null,
  "$0 時間"
 ],
 "$0 is not available from any repository.": [
  null,
  "$0 は、あらゆるリポジトリーから利用できません。"
 ],
 "$0 key changed": [
  null,
  "$0 キーが変更されました"
 ],
 "$0 killed with signal $1": [
  null,
  "$0 がシグナル $1 で終了しました"
 ],
 "$0 minute": [
  null,
  "$0 分"
 ],
 "$0 month": [
  null,
  "$0 カ月"
 ],
 "$0 week": [
  null,
  "$0 週"
 ],
 "$0 will be installed.": [
  null,
  "$0 がインストールされます。"
 ],
 "$0 year": [
  null,
  "$0 年"
 ],
 "1 day": [
  null,
  "1 日"
 ],
 "1 hour": [
  null,
  "1 時間"
 ],
 "1 minute": [
  null,
  "1 分"
 ],
 "1 week": [
  null,
  "1 週間"
 ],
 "20 minutes": [
  null,
  "20 分"
 ],
 "40 minutes": [
  null,
  "40 分"
 ],
 "5 minutes": [
  null,
  "5 分"
 ],
 "6 hours": [
  null,
  "6 時間"
 ],
 "60 minutes": [
  null,
  "60 分"
 ],
 "A modern browser is required for security, reliability, and performance.": [
  null,
  "セキュリティー、信頼性、およびパフォーマンスに対して最新のブラウザーが必要です。"
 ],
 "Absent": [
  null,
  "不在"
 ],
 "Accept key and log in": [
  null,
  "キーを受け入れてログイン"
 ],
 "Add $0": [
  null,
  "$0 の追加"
 ],
 "Additional packages:": [
  null,
  "追加のパッケージ:"
 ],
 "Administration with Cockpit Web Console": [
  null,
  "Cockpit Web コンソールでの管理"
 ],
 "Advanced TCA": [
  null,
  "高度な TCA"
 ],
 "All-in-one": [
  null,
  "オールインワン"
 ],
 "Ansible": [
  null,
  "Ansible"
 ],
 "Ansible roles documentation": [
  null,
  "Ansible ロールのドキュメント"
 ],
 "Authentication failed": [
  null,
  "認証に失敗しました"
 ],
 "Authentication failed: Server closed connection": [
  null,
  "認証に失敗しました: サーバーの接続が切断されました"
 ],
 "Authentication is required to perform privileged tasks with the Cockpit Web Console": [
  null,
  "Cockpit Web コンソールでの特権タスクの実行には、認証が必要です"
 ],
 "Automatically using NTP": [
  null,
  "NTP を自動的に使用"
 ],
 "Automatically using additional NTP servers": [
  null,
  "追加の NTP サーバーを自動的に使用"
 ],
 "Automatically using specific NTP servers": [
  null,
  "特定の NTP サーバーを自動的に使用"
 ],
 "Automation script": [
  null,
  "オートメーションスクリプト"
 ],
 "Blade": [
  null,
  "ブレード"
 ],
 "Blade enclosure": [
  null,
  "ブレードエンクロージャー"
 ],
 "Bus expansion chassis": [
  null,
  "バス拡張シャーシ"
 ],
 "Cancel": [
  null,
  "取り消し"
 ],
 "Cannot forward login credentials": [
  null,
  "ログインのクレデンシャルをフォワードできません"
 ],
 "Cannot schedule event in the past": [
  null,
  "過去のイベントはスケジュールできません"
 ],
 "Change": [
  null,
  "変更"
 ],
 "Change system time": [
  null,
  "システム時間の変更"
 ],
 "Changed keys are often the result of an operating system reinstallation. However, an unexpected change may indicate a third-party attempt to intercept your connection.": [
  null,
  "キーを変更すると、オペレーティングシステムを再インストールすることになることが多くあります。ただし、予期しない変更により接続の傍受が試行される場合もあります。"
 ],
 "Checking installed software": [
  null,
  "インストールされたソフトウェアの確認中"
 ],
 "Close": [
  null,
  "閉じる"
 ],
 "Cockpit": [
  null,
  "Cockpit"
 ],
 "Cockpit authentication is configured incorrectly.": [
  null,
  "Cockpit の認証が間違って設定されています。"
 ],
 "Cockpit configuration of NetworkManager and Firewalld": [
  null,
  "Cockpit の NetworkManager と Firewalld の設定"
 ],
 "Cockpit could not contact the given host.": [
  null,
  "Cockpit は該当するホストに接続できませんでした。"
 ],
 "Cockpit is a server manager that makes it easy to administer your Linux servers via a web browser. Jumping between the terminal and the web tool is no problem. A service started via Cockpit can be stopped via the terminal. Likewise, if an error occurs in the terminal, it can be seen in the Cockpit journal interface.": [
  null,
  "Cockpit は、Web ブラウザーで Linux サーバーを簡単に管理できるサーバーマネージャーです。端末と Web ツールを区別せずに使用できます。Cockpit で起動されたサービスは端末で停止できます。同様に、端末でエラーが発生した場合は、そのエラーを Cockpit ジャーナルインターフェースで確認できます。"
 ],
 "Cockpit is not compatible with the software on the system.": [
  null,
  "Cockpit にはシステム上のそのソフトウェアとの互換性がありません。"
 ],
 "Cockpit is not installed on the system.": [
  null,
  "Cockpit はシステムにインストールされていません。"
 ],
 "Cockpit is perfect for new sysadmins, allowing them to easily perform simple tasks such as storage administration, inspecting journals and starting and stopping services. You can monitor and administer several servers at the same time. Just add them with a single click and your machines will look after its buddies.": [
  null,
  "Cockpit は経験が少ないシステム管理者に最適です。これらのシステム管理者はストレージの管理、ジャーナルの検査、サービスの起動および停止などの単純なタスクを簡単に実行できるようになります。また、複数のサーバーを同時に監視および管理できます。これらのサーバーはクリックするだけで追加できます。追加後に、ご使用のマシンは他のマシンを管理するようになります。"
 ],
 "Cockpit might not render correctly in your browser": [
  null,
  "Cockpit がお使いのブラウザーで正しくレンダリングされない可能性があります"
 ],
 "Collect and package diagnostic and support data": [
  null,
  "診断およびサポートデータの収集とパッケージ化"
 ],
 "Collect kernel crash dumps": [
  null,
  "カーネルクラッシュダンプの収集"
 ],
 "Compact PCI": [
  null,
  "PCI の圧縮"
 ],
 "Connect to": [
  null,
  "接続先"
 ],
 "Connect to:": [
  null,
  "接続先:"
 ],
 "Connection has timed out.": [
  null,
  "接続がタイムアウトしました。"
 ],
 "Convertible": [
  null,
  "変換可能"
 ],
 "Copy": [
  null,
  "コピー"
 ],
 "Copy to clipboard": [
  null,
  "クリップボードにコピー"
 ],
 "Create": [
  null,
  "作成"
 ],
 "Create new task file with this content.": [
  null,
  "このコンテンツで新しいタスクファイルを作成します。"
 ],
 "Ctrl+Insert": [
  null,
  "Ctrl+Insert"
 ],
 "Delay": [
  null,
  "遅延"
 ],
 "Desktop": [
  null,
  "デスクトップ"
 ],
 "Detachable": [
  null,
  "割り当て解除可能"
 ],
 "Diagnostic reports": [
  null,
  "診断レポート"
 ],
 "Docking station": [
  null,
  "ドッキングステーション"
 ],
 "Download a new browser for free": [
  null,
  "新しいブラウザーを無料でダウンロードします"
 ],
 "Downloading $0": [
  null,
  "$0 をダウンロード中"
 ],
 "Dual rank": [
  null,
  "デュアルランク"
 ],
 "Embedded PC": [
  null,
  "組み込み PC"
 ],
 "Excellent password": [
  null,
  "優れたパスワード"
 ],
 "Expansion chassis": [
  null,
  "拡張シャーシ"
 ],
 "Failed to change password": [
  null,
  "パスワードの変更に失敗しました"
 ],
 "Failed to enable $0 in firewalld": [
  null,
  "firewalld での $0 の有効化に失敗しました"
 ],
 "Go to now": [
  null,
  "今すぐ移動"
 ],
 "Handheld": [
  null,
  "ハンドヘルド"
 ],
 "Host key is incorrect": [
  null,
  "ホスト鍵が正しくありません"
 ],
 "If the fingerprint matches, click \"Accept key and log in\". Otherwise, do not log in and contact your administrator.": [
  null,
  "フィンガープリントが一致する場合は、Accept key and login をクリックします。一致しない場合は、ログインせずに、管理者にお問い合わせください。"
 ],
 "Install": [
  null,
  "インストール"
 ],
 "Install software": [
  null,
  "ソフトウェアをインストール"
 ],
 "Installing $0": [
  null,
  "$0 をインストール中"
 ],
 "Internal error": [
  null,
  "内部エラー"
 ],
 "Internal error: Invalid challenge header": [
  null,
  "内部エラー: 無効なチャレンジヘッダー"
 ],
 "Invalid date format": [
  null,
  "無効な日付形式"
 ],
 "Invalid date format and invalid time format": [
  null,
  "無効な日付形式と無効な時間形式"
 ],
 "Invalid file permissions": [
  null,
  "無効なファイルパーミッション"
 ],
 "Invalid time format": [
  null,
  "無効な時間形式"
 ],
 "Invalid timezone": [
  null,
  "無効なタイムゾーン"
 ],
 "IoT gateway": [
  null,
  "IoT ゲートウェイ"
 ],
 "Kernel dump": [
  null,
  "カーネルダンプ"
 ],
 "Laptop": [
  null,
  "ラップトップ"
 ],
 "Learn more": [
  null,
  "もっと詳しく"
 ],
 "Loading system modifications...": [
  null,
  "システム変更をロード中..."
 ],
 "Log in": [
  null,
  "ログイン"
 ],
 "Log in with your server user account.": [
  null,
  "サーバーのユーザーアカウントでログインします。"
 ],
 "Log messages": [
  null,
  "ログメッセージ"
 ],
 "Login": [
  null,
  "ログイン"
 ],
 "Login again": [
  null,
  "再ログイン"
 ],
 "Login failed": [
  null,
  "ログインが失敗しました"
 ],
 "Logout successful": [
  null,
  "ログアウトが正常に行われました"
 ],
 "Low profile desktop": [
  null,
  "低プロファイルデスクトップ"
 ],
 "Lunch box": [
  null,
  "ランチボックス"
 ],
 "Main server chassis": [
  null,
  "メインサーバーシャーシ"
 ],
 "Manage storage": [
  null,
  "ストレージの管理"
 ],
 "Manually": [
  null,
  "手動"
 ],
 "Message to logged in users": [
  null,
  "ログインしているユーザーへのメッセージ"
 ],
 "Mini PC": [
  null,
  "ミニ PC"
 ],
 "Mini tower": [
  null,
  "ミニタワー"
 ],
 "Multi-system chassis": [
  null,
  "マルチシステムシャーシ"
 ],
 "NTP server": [
  null,
  "NTP サーバー"
 ],
 "Need at least one NTP server": [
  null,
  "少なくとも 1 つの NTP サーバーが必要です"
 ],
 "Networking": [
  null,
  "ネットワーキング"
 ],
 "New host": [
  null,
  "新規ホスト"
 ],
 "New password was not accepted": [
  null,
  "新規パスワードは受け入れられませんでした"
 ],
 "No delay": [
  null,
  "遅延なし"
 ],
 "No such file or directory": [
  null,
  "このようなファイルまたはディレクトリーがありません"
 ],
 "No system modifications": [
  null,
  "システム変更がありません"
 ],
 "Not a valid private key": [
  null,
  "有効な秘密鍵ではありません"
 ],
 "Not permitted to perform this action.": [
  null,
  "この動作を実行する権限がありません。"
 ],
 "Not synchronized": [
  null,
  "同期されていません"
 ],
 "Notebook": [
  null,
  "ノートブック"
 ],
 "Occurrences": [
  null,
  "発生"
 ],
 "Ok": [
  null,
  "OK"
 ],
 "Old password not accepted": [
  null,
  "古いパスワードは受け入れられません"
 ],
 "Once Cockpit is installed, enable it with \"systemctl enable --now cockpit.socket\".": [
  null,
  "Cockpit がインストールされたら、\"systemctl enable --now cockpit.socket\" コマンドで有効にします。"
 ],
 "Or use a bundled browser": [
  null,
  "あるいは、バンドルされたブラウザーを使用します"
 ],
 "Other": [
  null,
  "その他"
 ],
 "Other options": [
  null,
  "他のオプション"
 ],
 "PackageKit crashed": [
  null,
  "PackageKit がクラッシュしました"
 ],
 "Password": [
  null,
  "パスワード"
 ],
 "Password is not acceptable": [
  null,
  "パスワードは受け入れられません"
 ],
 "Password is too weak": [
  null,
  "パスワードが弱すぎます"
 ],
 "Password not accepted": [
  null,
  "パスワードは受け入れられません"
 ],
 "Paste": [
  null,
  "貼り付け"
 ],
 "Paste error": [
  null,
  "貼り付けエラー"
 ],
 "Path to file": [
  null,
  "ファイルのパス"
 ],
 "Peripheral chassis": [
  null,
  "周辺機器シャーシ"
 ],
 "Permission denied": [
  null,
  "パーミッションが拒否されました"
 ],
 "Pick date": [
  null,
  "日付けの選択"
 ],
 "Pizza box": [
  null,
  "Pizza box"
 ],
 "Please enable JavaScript to use the Web Console.": [
  null,
  "Web コンソールを使用するには JavaScript を有効にしてください。"
 ],
 "Please specify the host to connect to": [
  null,
  "接続するホストを指定してください"
 ],
 "Portable": [
  null,
  "ポータブル"
 ],
 "Present": [
  null,
  "存在"
 ],
 "Prompting via ssh-add timed out": [
  null,
  "ssh-add によるプロンプトがタイムアウトしました"
 ],
 "Prompting via ssh-keygen timed out": [
  null,
  "ssh-keygen によるプロンプトがタイムアウトしました"
 ],
 "RAID chassis": [
  null,
  "RAID シャーシ"
 ],
 "Rack mount chassis": [
  null,
  "ラックマウントシャーシ"
 ],
 "Reboot": [
  null,
  "再起動"
 ],
 "Recent hosts": [
  null,
  "直近のホスト"
 ],
 "Refusing to connect. Host is unknown": [
  null,
  "接続を拒否しています。ホストが不明です"
 ],
 "Refusing to connect. Hostkey does not match": [
  null,
  "接続を拒否しています。ホストキーが一致しません"
 ],
 "Refusing to connect. Hostkey is unknown": [
  null,
  "接続を拒否しています。ホストキーが不明です"
 ],
 "Removals:": [
  null,
  "削除:"
 ],
 "Remove host": [
  null,
  "ホストの削除"
 ],
 "Removing $0": [
  null,
  "$0 を削除中"
 ],
 "SELinux": [
  null,
  "SELinux"
 ],
 "Sealed-case PC": [
  null,
  "シールドケース PC"
 ],
 "Security Enhanced Linux configuration and troubleshooting": [
  null,
  "Security Enhanced Linux の設定とトラブルシューティング"
 ],
 "Server": [
  null,
  "サーバー"
 ],
 "Server has closed the connection.": [
  null,
  "サーバーの接続が終了しました。"
 ],
 "Set time": [
  null,
  "時間の設定"
 ],
 "Shell script": [
  null,
  "シェルスクリプト"
 ],
 "Shift+Insert": [
  null,
  "Shift+Insert"
 ],
 "Shut down": [
  null,
  "シャットダウン"
 ],
 "Single rank": [
  null,
  "シングルランク"
 ],
 "Space-saving computer": [
  null,
  "省スペースコンピューター"
 ],
 "Specific time": [
  null,
  "特定の時間"
 ],
 "Stick PC": [
  null,
  "スティッキー PC"
 ],
 "Storage": [
  null,
  "ストレージ"
 ],
 "Sub-Chassis": [
  null,
  "サブシャーシ"
 ],
 "Sub-Notebook": [
  null,
  "サブノート"
 ],
 "Synchronized": [
  null,
  "同期済み"
 ],
 "Synchronized with $0": [
  null,
  "$0 と同期済み"
 ],
 "Synchronizing": [
  null,
  "同期中"
 ],
 "Tablet": [
  null,
  "タブレット"
 ],
 "The logged in user is not permitted to view system modifications": [
  null,
  "ログインしているユーザーには、システム変更を表示する権限がありません"
 ],
 "The passwords do not match.": [
  null,
  "パスワードが一致しません。"
 ],
 "The resulting fingerprint is fine to share via public methods, including email.": [
  null,
  "作成されたフィンガープリントは、電子メールを含むパブリックメソッドを介して共有すると問題ありません。"
 ],
 "The server refused to authenticate '$0' using password authentication, and no other supported authentication methods are available.": [
  null,
  "サーバーはパスワード認証を使用した '$0' の認証を拒否しました。サポートされた他の認証方法は利用できません。"
 ],
 "The server refused to authenticate using any supported methods.": [
  null,
  "サーバーはサポートされた方法を使用した認証を拒否しました。"
 ],
 "The web browser configuration prevents Cockpit from running (inaccessible $0)": [
  null,
  "Web ブラウザーの設定により、Cockpit の実行は防がれます (アクセスできない $0)"
 ],
 "This tool configures the SELinux policy and can help with understanding and resolving policy violations.": [
  null,
  "このツールは、SELinux ポリシーを設定します。また、ポリシー違反の把握と解決に役立ちます。"
 ],
 "This tool configures the system to write kernel crash dumps to disk.": [
  null,
  "このツールは、カーネルクラッシュダンプをディスクに書き込むようにシステムを設定します。"
 ],
 "This tool generates an archive of configuration and diagnostic information from the running system. The archive may be stored locally or centrally for recording or tracking purposes or may be sent to technical support representatives, developers or system administrators to assist with technical fault-finding and debugging.": [
  null,
  "このツールは、実行中のシステムから設定および診断情報のアーカイブを生成します。アーカイブは、記録や追跡の目的でローカルまたは一元的に保存することも、技術的な障害の発見やデバッグを支援するためにテクニカルサポート担当者、開発者、システム管理者に送信することもできます。"
 ],
 "This tool manages local storage, such as filesystems, LVM2 volume groups, and NFS mounts.": [
  null,
  "このツールは、ファイルシステム、LVM2 ボリュームグループ、NFS マウントなどのローカルストレージを管理します。"
 ],
 "This tool manages networking such as bonds, bridges, teams, VLANs and firewalls using NetworkManager and Firewalld. NetworkManager is incompatible with Ubuntu's default systemd-networkd and Debian's ifupdown scripts.": [
  null,
  "このツールは、NetworkManager と Firewalld を使用して、ボンディング、ブリッジ、チーム、VLAN、ファイアウォールなどのネットワーク設定を管理します。NetworkManager は、Ubuntu のデフォルトの systemd-networkd および Debian の ifupdown スクリプトと互換性がありません。"
 ],
 "This web browser is too old to run the Web Console (missing $0)": [
  null,
  "この Web ブラウザーは古いため、Web コンソールを実行できません ($0 が不明)"
 ],
 "Time zone": [
  null,
  "タイムゾーン"
 ],
 "To ensure that your connection is not intercepted by a malicious third-party, please verify the host key fingerprint:": [
  null,
  "悪意のあるサードパーティーによって接続がインターセプトされないようにするには、ホストキーフィンガープリントを確認してください:"
 ],
 "To verify a fingerprint, run the following on $0 while physically sitting at the machine or through a trusted network:": [
  null,
  "フィンガープリントを確認するには、マシン上に物理的に置かれるか、信頼できるネットワークを介して $0 で次のコマンドを実行します:"
 ],
 "Toggle date picker": [
  null,
  "日付選択の切り替え"
 ],
 "Too much data": [
  null,
  "データが多すぎます"
 ],
 "Total size: $0": [
  null,
  "合計サイズ: $0"
 ],
 "Tower": [
  null,
  "タワー"
 ],
 "Try again": [
  null,
  "再試行します"
 ],
 "Trying to synchronize with $0": [
  null,
  "$0 との同期を試行中です"
 ],
 "Unable to connect to that address": [
  null,
  "そのアドレスに接続できません"
 ],
 "Unknown": [
  null,
  "不明"
 ],
 "Untrusted host": [
  null,
  "信用できないホスト"
 ],
 "User name": [
  null,
  "ユーザー名"
 ],
 "User name cannot be empty": [
  null,
  "ユーザー名は空にできません"
 ],
 "Validating authentication token": [
  null,
  "認証トークンの検証"
 ],
 "View all logs": [
  null,
  "すべてのログの表示"
 ],
 "View automation script": [
  null,
  "オートメーションスクリプトの表示"
 ],
 "Visit firewall": [
  null,
  "ファイアウォールへのアクセス"
 ],
 "Waiting for other software management operations to finish": [
  null,
  "他のソフトウェア管理オペレーションが終了するまで待機中"
 ],
 "Web Console for Linux servers": [
  null,
  "Linux サーバー用 Web コンソール"
 ],
 "Wrong user name or password": [
  null,
  "ユーザー名またはパスワードが間違っています"
 ],
 "You are connecting to $0 for the first time.": [
  null,
  "初めて $0 に接続しています。"
 ],
 "Your browser does not allow paste from the context menu. You can use Shift+Insert.": [
  null,
  "お使いのブラウザーでは、コンテキストメニューからの貼り付けが許可されていません。Shift+Insert を使用できます。"
 ],
 "Your session has been terminated.": [
  null,
  "セッションが終了しました。"
 ],
 "Your session has expired. Please log in again.": [
  null,
  "セッションの有効期限が切れました。再度ログインしてください。"
 ],
 "Zone": [
  null,
  "ゾーン"
 ],
 "[binary data]": [
  null,
  "[バイナリーデータ]"
 ],
 "[no data]": [
  null,
  "[データなし]"
 ],
 "password quality": [
  null,
  "パスワードの強度"
 ],
 "show less": [
  null,
  "簡易表示"
 ],
 "show more": [
  null,
  "詳細表示"
 ]
};
