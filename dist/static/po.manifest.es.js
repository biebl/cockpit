window.cockpit_po = {
 "": {
  "plural-forms": (n) => n != 1,
  "language": "es",
  "language-direction": "ltr"
 },
 "Diagnostic reports": [
  null,
  "Informes de diagnóstico"
 ],
 "Kernel dump": [
  null,
  "Volcado del kernel"
 ],
 "Networking": [
  null,
  "Redes"
 ],
 "SELinux": [
  null,
  "SELinux"
 ],
 "Storage": [
  null,
  "Almacenamiento"
 ]
};
