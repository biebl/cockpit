window.cockpit_po = {
 "": {
  "plural-forms": (n) => n != 1,
  "language": "de",
  "language-direction": "ltr"
 },
 "$0 GiB": [
  null,
  "$0 GiB"
 ],
 "$0 day": [
  null,
  "$0 Tag",
  "$0 Tage"
 ],
 "$0 error": [
  null,
  "$0 Error"
 ],
 "$0 exited with code $1": [
  null,
  "$0 mit Code $1 beendet"
 ],
 "$0 failed": [
  null,
  "$0 fehlgeschlagen"
 ],
 "$0 hour": [
  null,
  "$0 Stunde",
  "$0 Stunden"
 ],
 "$0 is not available from any repository.": [
  null,
  "$0 ist in keinem Repository verfügbar."
 ],
 "$0 key changed": [
  null,
  "$0 Schlüssel geändert"
 ],
 "$0 killed with signal $1": [
  null,
  "$0 mit Signal $1 beendet"
 ],
 "$0 minute": [
  null,
  "$0 Minute",
  "$0 Minuten"
 ],
 "$0 month": [
  null,
  "$0 Monat",
  "$0 Monate"
 ],
 "$0 week": [
  null,
  "$0 Woche",
  "$0 Wochen"
 ],
 "$0 will be installed.": [
  null,
  "$0 wird installiert."
 ],
 "$0 year": [
  null,
  "$0 Jahr",
  "$0 Jahre"
 ],
 "1 day": [
  null,
  "1 Tag"
 ],
 "1 hour": [
  null,
  "1 Stunde"
 ],
 "1 minute": [
  null,
  "1 Minute"
 ],
 "1 week": [
  null,
  "1 Woche"
 ],
 "20 minutes": [
  null,
  "20 Minuten"
 ],
 "40 minutes": [
  null,
  "40 Minuten"
 ],
 "5 minutes": [
  null,
  "5 Minuten"
 ],
 "6 hours": [
  null,
  "6 Stunden"
 ],
 "60 minutes": [
  null,
  "60 Minuten"
 ],
 "A modern browser is required for security, reliability, and performance.": [
  null,
  "Ein moderner Browser ist für Sicherheit, Zuverlässigkeit und Leistung erforderlich."
 ],
 "Absent": [
  null,
  "Abwesend"
 ],
 "Accept key and log in": [
  null,
  "Schlüssel akzeptieren und anmelden"
 ],
 "Add $0": [
  null,
  "$0 hinzufügen"
 ],
 "Additional packages:": [
  null,
  "Zusatzpakete:"
 ],
 "Administration with Cockpit Web Console": [
  null,
  "Mit der Cockpit Web Konsole administrieren"
 ],
 "Advanced TCA": [
  null,
  "Fortgeschrittenes TCA"
 ],
 "All-in-one": [
  null,
  "Alles-in-einem"
 ],
 "Ansible": [
  null,
  "Ansible"
 ],
 "Ansible roles documentation": [
  null,
  "Ansible Rollendokumentation"
 ],
 "Authentication failed": [
  null,
  "Authentifizierung fehlgeschlagen"
 ],
 "Authentication failed: Server closed connection": [
  null,
  "Authentifizierung fehlgeschlagen: Server geschlossene Verbindung"
 ],
 "Authentication is required to perform privileged tasks with the Cockpit Web Console": [
  null,
  "Privilegierte Aktionen der Cockpit Web-Konsole benötigen Berechtigung"
 ],
 "Automatically using NTP": [
  null,
  "Automatisch (NTP)"
 ],
 "Automatically using additional NTP servers": [
  null,
  "Automatische Benutzung zusätzlicher NTP-Server"
 ],
 "Automatically using specific NTP servers": [
  null,
  "Automatisch (spezifische NTP-Server)"
 ],
 "Automation script": [
  null,
  "Automatisierungs-Skript"
 ],
 "Blade": [
  null,
  "Blade"
 ],
 "Blade enclosure": [
  null,
  "Bladegehäuse"
 ],
 "Bus expansion chassis": [
  null,
  "Bus-Erweiterungsgehäuse"
 ],
 "Cancel": [
  null,
  "Abbrechen"
 ],
 "Cannot forward login credentials": [
  null,
  "Anmeldeinformationen können nicht weitergeleitet werden"
 ],
 "Cannot schedule event in the past": [
  null,
  "Vorgang kann nicht für die Vergangenheit geplant werden"
 ],
 "Change": [
  null,
  "Ändern"
 ],
 "Change system time": [
  null,
  "Systemzeit ändern"
 ],
 "Changed keys are often the result of an operating system reinstallation. However, an unexpected change may indicate a third-party attempt to intercept your connection.": [
  null,
  "Geänderte Schlüssel sind oft das Ergebnis einer Neuinstallation des Betriebssystems. Allerdings kann eine unerwartete Änderung auf einen Versuch eines Dritten hinweisen, Ihre Verbindung auszuspähen."
 ],
 "Checking installed software": [
  null,
  "Installierte Software wird überprüft"
 ],
 "Close": [
  null,
  "Schließen"
 ],
 "Cockpit": [
  null,
  "Cockpit"
 ],
 "Cockpit authentication is configured incorrectly.": [
  null,
  "Die Cockpit-Authentifizierung ist falsch konfiguriert."
 ],
 "Cockpit configuration of NetworkManager and Firewalld": [
  null,
  "Cockpit Konfiguration von NetworkManager und Firewalld"
 ],
 "Cockpit could not contact the given host.": [
  null,
  "Cockpit konnte den angegebenen Host nicht erreichen."
 ],
 "Cockpit is a server manager that makes it easy to administer your Linux servers via a web browser. Jumping between the terminal and the web tool is no problem. A service started via Cockpit can be stopped via the terminal. Likewise, if an error occurs in the terminal, it can be seen in the Cockpit journal interface.": [
  null,
  "Cockpit ist ein Server Manager zur einfachen Verwaltung Ihrer Linux Server via Web Browser. Ein Wechsel zwischen dem Terminal und der Weboberfläche ist kein Problem. Ein Service, der via Cockpit gestartet wurde, kann im Terminal beendet werden. Genauso können Fehler, welche im Terminal vorkommen, im Cockpit Journal angezeigt werden."
 ],
 "Cockpit is not compatible with the software on the system.": [
  null,
  "Cockpit ist mit der Software auf dem System nicht kompatibel."
 ],
 "Cockpit is not installed on the system.": [
  null,
  "Cockpit ist auf dem System nicht installiert."
 ],
 "Cockpit is perfect for new sysadmins, allowing them to easily perform simple tasks such as storage administration, inspecting journals and starting and stopping services. You can monitor and administer several servers at the same time. Just add them with a single click and your machines will look after its buddies.": [
  null,
  "Cockpit ist perfekt für neue Systemadministratoren, da es ihnen auf einfache Weise ermöglicht, simple Aufgaben wie Speicherverwaltung, Journal / Logfile Analyse oder das Starten und Stoppen von Diensten durchzuführen. Sie können gleichzeitig mehrere Server überwachen und verwalten. Fügen Sie weitere Maschinen mit einem Klick hinzu und Ihre Maschinen schauen zu ihren Kumpels."
 ],
 "Cockpit might not render correctly in your browser": [
  null,
  "Cockpit wird in Ihrem Browser möglicherweise nicht korrekt dargestellt"
 ],
 "Collect and package diagnostic and support data": [
  null,
  "Sammeln und Packen von Diagnose und Support Daten"
 ],
 "Collect kernel crash dumps": [
  null,
  "Sammeln von Kernel-Absturz-Auszügen"
 ],
 "Compact PCI": [
  null,
  "Kompakte PCI"
 ],
 "Connect to": [
  null,
  "Verbinden zu"
 ],
 "Connect to:": [
  null,
  "Verbinden mit:"
 ],
 "Connection has timed out.": [
  null,
  "Zeitüberschreitung bei der Verbindung."
 ],
 "Convertible": [
  null,
  "Convertible"
 ],
 "Copy": [
  null,
  "Kopieren"
 ],
 "Copy to clipboard": [
  null,
  "In Zwischenablage kopieren"
 ],
 "Create": [
  null,
  "Erstellen"
 ],
 "Create new task file with this content.": [
  null,
  "Neue Task-Datei mit diesem Inhalt erstellen."
 ],
 "Ctrl+Insert": [
  null,
  "Strg+Einfügen"
 ],
 "Delay": [
  null,
  "Verzögerung"
 ],
 "Desktop": [
  null,
  "Desktop"
 ],
 "Detachable": [
  null,
  "Abnehmbar"
 ],
 "Diagnostic reports": [
  null,
  "Diagnoseberichte"
 ],
 "Docking station": [
  null,
  "Dockingstation"
 ],
 "Download a new browser for free": [
  null,
  "Laden Sie kostenlos einen neuen Browser herunter"
 ],
 "Downloading $0": [
  null,
  "wird heruntergeladen $0"
 ],
 "Dual rank": [
  null,
  "Doppelter Rang"
 ],
 "Embedded PC": [
  null,
  "Embedded PC"
 ],
 "Excellent password": [
  null,
  "Perfektes Passwort"
 ],
 "Expansion chassis": [
  null,
  "Erweiterungsgehäuse"
 ],
 "Failed to change password": [
  null,
  "Passwort konnte nicht geändert werden"
 ],
 "Failed to enable $0 in firewalld": [
  null,
  "$0 konnte nicht in firewalld aktiviert werden"
 ],
 "Go to now": [
  null,
  "Zu 'Jetzt' gehen"
 ],
 "Handheld": [
  null,
  "Handheld"
 ],
 "Host key is incorrect": [
  null,
  "Host-Schlüssel ist falsch"
 ],
 "If the fingerprint matches, click \"Accept key and log in\". Otherwise, do not log in and contact your administrator.": [
  null,
  "Wenn der Fingerabdruck übereinstimmt, klicke \"Schlüssel akzeptieren und einloggen\". Andernfalls, Login abbrechen und den Administrator kontaktieren."
 ],
 "Install": [
  null,
  "Installation"
 ],
 "Install software": [
  null,
  "Software installieren"
 ],
 "Installing $0": [
  null,
  "$0 wird installiert"
 ],
 "Internal error": [
  null,
  "Interner Fehler"
 ],
 "Internal error: Invalid challenge header": [
  null,
  "Interner Fehler: Ungültiger Challenge-Header"
 ],
 "Invalid date format": [
  null,
  "Ungültiges Datumsformat"
 ],
 "Invalid date format and invalid time format": [
  null,
  "Ungültiges Datumsformat und ungültiges Zeitformat"
 ],
 "Invalid file permissions": [
  null,
  "Ungültige Dateiberechtigungen"
 ],
 "Invalid time format": [
  null,
  "Ungültiges Zeitformat"
 ],
 "Invalid timezone": [
  null,
  "Ungültige Zeitzone"
 ],
 "IoT gateway": [
  null,
  "IoT-Gateway"
 ],
 "Kernel dump": [
  null,
  "Kernel dump"
 ],
 "Laptop": [
  null,
  "Laptop"
 ],
 "Learn more": [
  null,
  "Mehr erfahren"
 ],
 "Loading system modifications...": [
  null,
  "System-Änderungen laden..."
 ],
 "Log in": [
  null,
  "Anmelden"
 ],
 "Log in with your server user account.": [
  null,
  "Melden Sie sich mit dem Server-Benutzerkonto an."
 ],
 "Log messages": [
  null,
  "Nachrichten protokollieren"
 ],
 "Login": [
  null,
  "Anmeldung"
 ],
 "Login again": [
  null,
  "Nochmal anmelden"
 ],
 "Login failed": [
  null,
  "Anmeldung fehlgeschlagen"
 ],
 "Logout successful": [
  null,
  "Abmeldung erfolgreich"
 ],
 "Low profile desktop": [
  null,
  "Low-Profile-Desktop"
 ],
 "Lunch box": [
  null,
  "Brotdose"
 ],
 "Main server chassis": [
  null,
  "Hauptservergehäuse"
 ],
 "Manage storage": [
  null,
  "Speicher verwalten"
 ],
 "Manually": [
  null,
  "Manuell"
 ],
 "Message to logged in users": [
  null,
  "Nachricht an angemeldete Benutzer"
 ],
 "Mini PC": [
  null,
  "Mini PC"
 ],
 "Mini tower": [
  null,
  "Mini-Tower"
 ],
 "Multi-system chassis": [
  null,
  "Multi-System-Chassis"
 ],
 "NTP server": [
  null,
  "NTP-Server"
 ],
 "Need at least one NTP server": [
  null,
  "Benötigen Sie mindestens einen NTP-Server"
 ],
 "Networking": [
  null,
  "Netzwerk"
 ],
 "New host": [
  null,
  "Neuer Host"
 ],
 "New password was not accepted": [
  null,
  "Das neue Passwort wurde nicht akzeptiert"
 ],
 "No delay": [
  null,
  "Keine Verzögerung"
 ],
 "No such file or directory": [
  null,
  "Datei oder Verzeichnis nicht vorhanden"
 ],
 "No system modifications": [
  null,
  "Keine Systemänderungen"
 ],
 "Not a valid private key": [
  null,
  "Ungültiger privater Schlüssel"
 ],
 "Not permitted to perform this action.": [
  null,
  "Diese Aktion darf nicht ausgeführt werden."
 ],
 "Not synchronized": [
  null,
  "Nicht synchronisiert"
 ],
 "Notebook": [
  null,
  "Notizbuch"
 ],
 "Occurrences": [
  null,
  "Vorkommnisse"
 ],
 "Ok": [
  null,
  "OK"
 ],
 "Old password not accepted": [
  null,
  "Altes Passwort wurde nicht akzeptiert"
 ],
 "Once Cockpit is installed, enable it with \"systemctl enable --now cockpit.socket\".": [
  null,
  "Wenn Cockpit installiert ist, aktivieren Sie es mit \"systemctl enable --now cockpit.socket\"."
 ],
 "Or use a bundled browser": [
  null,
  "Oder verwenden Sie einen gebündelten Browser"
 ],
 "Other": [
  null,
  "Weitere"
 ],
 "Other options": [
  null,
  "Andere Einstellungen"
 ],
 "PackageKit crashed": [
  null,
  "PackageKit ist abgestürzt"
 ],
 "Password": [
  null,
  "Passwort"
 ],
 "Password is not acceptable": [
  null,
  "Das Passwort kann nicht akzeptiert werden"
 ],
 "Password is too weak": [
  null,
  "Das gewählte Passwort ist zu schwach"
 ],
 "Password not accepted": [
  null,
  "Passwort wurde nicht akzeptiert"
 ],
 "Paste": [
  null,
  "Einfügen"
 ],
 "Paste error": [
  null,
  "Fehler beim Einfügen"
 ],
 "Path to file": [
  null,
  "Pfad zur Datei"
 ],
 "Peripheral chassis": [
  null,
  "Peripheriechassis"
 ],
 "Permission denied": [
  null,
  "Erlaubnis verweigert"
 ],
 "Pick date": [
  null,
  "Datum auswählen"
 ],
 "Pizza box": [
  null,
  "Pizza-Box"
 ],
 "Please enable JavaScript to use the Web Console.": [
  null,
  "Bitte aktivieren Sie JavaScript, um die Web-Konsole verwenden zu können."
 ],
 "Please specify the host to connect to": [
  null,
  "Bitte geben Sie den Host an, zu dem eine Verbindung hergestellt werden soll"
 ],
 "Portable": [
  null,
  "tragbar"
 ],
 "Present": [
  null,
  "Derzeit"
 ],
 "Prompting via ssh-add timed out": [
  null,
  "Die Aufforderung über ssh-add ist abgelaufen"
 ],
 "Prompting via ssh-keygen timed out": [
  null,
  "Die Aufforderung über ssh-keygen ist abgelaufen"
 ],
 "RAID chassis": [
  null,
  "RAID-Chassis"
 ],
 "Rack mount chassis": [
  null,
  "Rack-Einbaugehäuse"
 ],
 "Reboot": [
  null,
  "Neustart"
 ],
 "Recent hosts": [
  null,
  "Zuletzt genutzte Hosts"
 ],
 "Refusing to connect. Host is unknown": [
  null,
  "Verbindung ablehnen Host ist unbekannt"
 ],
 "Refusing to connect. Hostkey does not match": [
  null,
  "Verbindung ablehnen Hostkey stimmt nicht überein"
 ],
 "Refusing to connect. Hostkey is unknown": [
  null,
  "Verbindung ablehnen Hostkey ist unbekannt"
 ],
 "Removals:": [
  null,
  "Umzüge:"
 ],
 "Remove host": [
  null,
  "Host entfernen"
 ],
 "Removing $0": [
  null,
  "Entfernen $0"
 ],
 "SELinux": [
  null,
  "SELinux"
 ],
 "Sealed-case PC": [
  null,
  "PC mit versiegeltem Gehäuse"
 ],
 "Security Enhanced Linux configuration and troubleshooting": [
  null,
  "Sicherheitsverstärkte Linuxkonfiguration und Problemlösung"
 ],
 "Server": [
  null,
  "Server"
 ],
 "Server has closed the connection.": [
  null,
  "Der Server hat die Verbindung beendet."
 ],
 "Set time": [
  null,
  "Zeit setzen"
 ],
 "Shell script": [
  null,
  "Shell script"
 ],
 "Shift+Insert": [
  null,
  "Shift+Einfügen"
 ],
 "Shut down": [
  null,
  "Herunterfahren"
 ],
 "Single rank": [
  null,
  "Einzelner Rang"
 ],
 "Space-saving computer": [
  null,
  "Platzsparender Computer"
 ],
 "Specific time": [
  null,
  "Bestimmte Zeit"
 ],
 "Stick PC": [
  null,
  "Stick PC"
 ],
 "Storage": [
  null,
  "Speicher"
 ],
 "Sub-Chassis": [
  null,
  "Sub-Chassis"
 ],
 "Sub-Notebook": [
  null,
  "Sub-Notebook"
 ],
 "Synchronized": [
  null,
  "Synchronisiert"
 ],
 "Synchronized with $0": [
  null,
  "Mit $0 synchronisiert"
 ],
 "Synchronizing": [
  null,
  "Wird synchronisiert"
 ],
 "Tablet": [
  null,
  "Tablett"
 ],
 "The logged in user is not permitted to view system modifications": [
  null,
  "Der angemeldete Benutzer ist nicht berechtigt, Systemänderungen einzusehen"
 ],
 "The passwords do not match.": [
  null,
  "Die Passwörter stimmen nicht überein."
 ],
 "The resulting fingerprint is fine to share via public methods, including email.": [
  null,
  "Der entstandene Fingerabdruck kann über öffentliche Methoden, einschließlich E-Mail, weitergegeben werden."
 ],
 "The server refused to authenticate '$0' using password authentication, and no other supported authentication methods are available.": [
  null,
  "Der Server hat die Authentifizierung abgelehnt. '$0Mit der Passwort-Authentifizierung stehen keine anderen unterstützten Authentifizierungsmethoden zur Verfügung."
 ],
 "The server refused to authenticate using any supported methods.": [
  null,
  "Der Server hat die Authentifizierung mit allen unterstützten Methoden abgelehnt."
 ],
 "The web browser configuration prevents Cockpit from running (inaccessible $0)": [
  null,
  "Die Konfiguration des Webbrowsers verhindert, dass Cockpit ausgeführt wird (nicht erreichbar) $0)"
 ],
 "This tool configures the SELinux policy and can help with understanding and resolving policy violations.": [
  null,
  "Dieses Tool konfiguriert die SELinux Policy und hilft dabei Verletzungen der Policy zu verstehen und aufzulösen."
 ],
 "This tool configures the system to write kernel crash dumps to disk.": [
  null,
  "Dieses Tool konfiguriert das System zum Schreiben von Kernel Absturz Auszügen auf Datenträger."
 ],
 "This tool generates an archive of configuration and diagnostic information from the running system. The archive may be stored locally or centrally for recording or tracking purposes or may be sent to technical support representatives, developers or system administrators to assist with technical fault-finding and debugging.": [
  null,
  "Dieses Tool generiert ein Archiv der Konfiguration und Diagnoseinformation des laufenden Systems.Das Archiv kann lokal oder zentral abgespeichert werden zum Zweck der Archivierung oder Nachverfolgung oder kann an den Technischen Support, Entwickler oder Systemadministratoren gesendet werden, um bei der Fehlersuche oder Debugging zu helfen."
 ],
 "This tool manages local storage, such as filesystems, LVM2 volume groups, and NFS mounts.": [
  null,
  "Dieses Tool verwaltet den lokalen Speicher, wie etwa Dateisysteme, LVM2 Volume Gruppen und NFS Einhängepunkte."
 ],
 "This tool manages networking such as bonds, bridges, teams, VLANs and firewalls using NetworkManager and Firewalld. NetworkManager is incompatible with Ubuntu's default systemd-networkd and Debian's ifupdown scripts.": [
  null,
  "Dieses Tool verwaltet die Netzwerkumgebung wie etwa Bindungen, Bridges, Teams, VLANs und Firewalls durch den NetworkManager und Firewalld. Der NetworkManager ist inkompatibel mit dem Ubuntus Standard systemd-networkd und Debians ifupdown Scipts."
 ],
 "This web browser is too old to run the Web Console (missing $0)": [
  null,
  "Dieser Webbrowser ist zu alt, um die Web-Konsole auszuführen (fehlende $0)"
 ],
 "Time zone": [
  null,
  "Zeitzone"
 ],
 "To ensure that your connection is not intercepted by a malicious third-party, please verify the host key fingerprint:": [
  null,
  "Überprüfen Sie bitte den Fingerabdruck des Host-Schlüssels, um sicherzustellen, dass Ihre Verbindung nicht von einem böswilligen Dritten ausgespäht wird:"
 ],
 "To verify a fingerprint, run the following on $0 while physically sitting at the machine or through a trusted network:": [
  null,
  "Um einen Fingerabdruck zu überprüfen, führen Sie die folgenden Schritte auf $0 aus, während Sie physisch an der Maschine sitzen oder über ein vertrauenswürdiges Netzwerk:"
 ],
 "Toggle date picker": [
  null,
  "Datumsauswahl umschalten"
 ],
 "Too much data": [
  null,
  "Zu viele Daten"
 ],
 "Total size: $0": [
  null,
  "Gesamtgröße: $0"
 ],
 "Tower": [
  null,
  "Turm"
 ],
 "Try again": [
  null,
  "Versuchen Sie es nochmal"
 ],
 "Trying to synchronize with $0": [
  null,
  "Versuche mit {{Server}} zu synchronisieren"
 ],
 "Unable to connect to that address": [
  null,
  "Es kann keine Verbindung zu dieser Adresse hergestellt werden"
 ],
 "Unknown": [
  null,
  "Unbekannt"
 ],
 "Untrusted host": [
  null,
  "Nicht vertrauenswürdiger Host"
 ],
 "User name": [
  null,
  "Benutzername"
 ],
 "User name cannot be empty": [
  null,
  "Der Benutzername darf nicht leer sein"
 ],
 "Validating authentication token": [
  null,
  "Authentifizierungstoken überprüfen"
 ],
 "View all logs": [
  null,
  "Alle Protokolle ansehen"
 ],
 "View automation script": [
  null,
  "Automatisierungs-Script anzeigen"
 ],
 "Visit firewall": [
  null,
  "Firewall besuchen"
 ],
 "Waiting for other software management operations to finish": [
  null,
  "Warten, bis andere Software-Verwaltungsvorgänge abgeschlossen sind"
 ],
 "Web Console for Linux servers": [
  null,
  "Webkonsole für Linux-Server"
 ],
 "Wrong user name or password": [
  null,
  "Benutzername oder Passwort falsch"
 ],
 "You are connecting to $0 for the first time.": [
  null,
  "Sie stellen zum ersten Mal eine Verbindung zu $0 her."
 ],
 "Your browser does not allow paste from the context menu. You can use Shift+Insert.": [
  null,
  "Ihr Browser lässt das Einfügen über das Kontextmenü nicht zu. Sie können Umschalt+Einfügen verwenden."
 ],
 "Your session has been terminated.": [
  null,
  "Ihre Sitzung wurde beendet."
 ],
 "Your session has expired. Please log in again.": [
  null,
  "Die Session ist abgelaufen. Bitte neu einloggen."
 ],
 "Zone": [
  null,
  "Zone"
 ],
 "[binary data]": [
  null,
  "[Binärdaten]"
 ],
 "[no data]": [
  null,
  "[keine Daten]"
 ],
 "password quality": [
  null,
  "Passwortqualität"
 ],
 "show less": [
  null,
  "zeige weniger"
 ],
 "show more": [
  null,
  "Zeig mehr"
 ]
};
