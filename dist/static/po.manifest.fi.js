window.cockpit_po = {
 "": {
  "plural-forms": (n) => n != 1,
  "language": "fi",
  "language-direction": "ltr"
 },
 "Diagnostic reports": [
  null,
  "Diagnostiikkaraportit"
 ],
 "Kernel dump": [
  null,
  "Ytimen tyhjennys"
 ],
 "Networking": [
  null,
  "Verkko"
 ],
 "SELinux": [
  null,
  "SELinux"
 ],
 "Storage": [
  null,
  "Tallennustila"
 ]
};
