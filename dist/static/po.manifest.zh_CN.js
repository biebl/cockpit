window.cockpit_po = {
 "": {
  "plural-forms": (n) => 0,
  "language": "zh_CN",
  "language-direction": "ltr"
 },
 "Diagnostic reports": [
  null,
  "诊断报告"
 ],
 "Kernel dump": [
  null,
  "内核转储"
 ],
 "Networking": [
  null,
  "网络"
 ],
 "SELinux": [
  null,
  "SELinux"
 ],
 "Storage": [
  null,
  "存储"
 ]
};
