window.cockpit_po = {
 "": {
  "plural-forms": (n) => n != 1,
  "language": "ka",
  "language-direction": "ltr"
 },
 "Diagnostic reports": [
  null,
  "დიაგნოსტიკის ანგარიშები"
 ],
 "Kernel dump": [
  null,
  "ბირთვის დამპი"
 ],
 "Networking": [
  null,
  "ქსელი"
 ],
 "SELinux": [
  null,
  "SELinux"
 ],
 "Storage": [
  null,
  "საცავი"
 ]
};
