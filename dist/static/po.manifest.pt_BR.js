window.cockpit_po = {
 "": {
  "plural-forms": (n) => (n != 1),
  "language": "pt_BR",
  "language-direction": "ltr"
 },
 "Diagnostic reports": [
  null,
  "Relatório de diagnostico"
 ],
 "Kernel dump": [
  null,
  "Dump do Kernel"
 ],
 "Networking": [
  null,
  "Rede"
 ],
 "SELinux": [
  null,
  "SELinux"
 ],
 "Storage": [
  null,
  "Armazenamento"
 ]
};
